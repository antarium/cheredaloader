#coding: utf8
"""
  модуль для тестов фильтрации и загрузки данных. на его основе потом 
  сделаю в ООП стиле
"""
import motor.motor_asyncio
import settings as conf
import asyncio
import pandas as pd
from pandas import ExcelFile
import numpy as np
import datetime
import time as TIME
import pprint


DB = conf.DATABASE['default']
COLLECTIONS = {
    'doctors': 'doctors',
    'org': 'organizations',
    'records': 'patientsList'
}

def split_docname(line):
    """
      на вход ФИО строкой через пробел
      порядок возвращаемых данных: last(фамилия)б first(имя), middle(отчество)
    """
    chunk = line.split(' ')
    if len(chunk) >= 3:
        return chunk[0], chunk[1], chunk[2]
    elif len(chunk) == 2:
        return chunk[0], chunk[1], ''
    elif len(chunk) == 1:
        return chunk[0], '', ''
    else:
        return '', '', ''

def convert_datetime(date, time):
        time = time.split('-')[0]
        full_date = datetime.datetime.strptime("{}-{}".format(date, time), '%d.%m.%y-%H:%M')
        utc_ts = TIME.mktime(full_date.utctimetuple())
        dt = datetime.datetime.utcfromtimestamp(utc_ts)

def convert_patname(middle, first, last):
    """
      из полученных отчества, имени и фамилии собираем запись вида фамилия И.О.
    """
    first = "{}.".format(first[0]) if first else ''
    middle = "{}.".format(middle[0]) if middle else ''
    return "{} {}{}".format(last.capitalize(), first.capitalize(), middle.capitalize())

def prepare_doctor_data(doctor_name, doctor_id):
    """
      подготавливает поле doctor queue и для записи в бд
    """
    return {'name': doctor_name, 'id': doctor_id}, {'type': 'plan'}

async def do_find_doctors(db, filter):
    """
      реализует запрос в бд, получение и зполнение doctor.id
    """
    print('***запрос в БД для получения doc.id {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    collection = db[COLLECTIONS['doctors']]
    columns = ('unit', 'doctor', 'code', 'spec', 'doctor.id')
    df_result = pd.DataFrame(columns=columns)
    i = 0
    async for document in collection.find({'$or': filter}):
        doc_id = str(document['_id'])
        name = ' '.join([document['name']['last'], document['name']['first'], 
                        document['name']['middle']])
        df_result.loc[i] = (document['squad'], name, 
                            document['organization']['reestr'],
                            document['specialty'], doc_id)
        i+=1
    return df_result

async def do_delete_records(db, filter, k):
    """
      k - множитель, поскольку записей за раз пытаемся удалить
    """
    print('***запрос в БД для удаления данных {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    coll = db[COLLECTIONS['records']]
    n = await coll.count_documents({})
    print('%s documents before calling delete_many()' % n)
    #здесь будем разбивать filter на удаление на части (иначе монга жалуется)
    start, end = 0, 0 
    item = [None]
    while(len(item) !=0):
        end = start+k
        item = filter[start:end]
        if(item):
            result = await coll.delete_many({'$or': item})
            print('%s documents after' % (await coll.count_documents({})))
        start = end

async def do_insert_records(db, data, k):
    """
      добавление данных в бд
    """
    print('***запрос в БД для добавления данных {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    collection = db[COLLECTIONS['records']]
    #здесь будем разбивать filter на удаление на части (иначе монга жалуется)
    start, end = 0, 0 
    item = [None]
    while(len(item) !=0):
        end = start+k
        item = data[start:end]
        if(item):
            result = await collection.insert_many(item)
            print('inserted %d docs' % (len(result.inserted_ids),))
        start = end

async def delete_records(db, df, k):
    """
      удаление записей с темже временем и к тому же доктору, что и в 
      загружаемом файле
      k - множитель, поскольку записей за раз пытаемся удалить
    """
    print('***готовим данные для удаления записей {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    vec_convert = np.frompyfunc(convert_datetime, 2, 1)
    df['record'] = vec_convert(df['date'], df['time'])
    df['date'] = datetime.datetime.strftime(datetime.datetime.today(), '%d.%m.%y')
    for_delete = df[['doctor.id', 'record']].to_dict('records')
    await do_delete_records(db, for_delete, k)
    return df

async def prepare_doctor_id(db, df):
    """
      на основе фрейма данных формирует условие фильтра, делает запрос в бд 
      и заполняет поле doctor.id в основном фрейме данных с использованием 
      do_find_doctors
    """
    print('***формируем фильтр для получения doc.id {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df_doc = df.drop_duplicates(['doctor', 'code', 'unit', 'spec'])
    df_doc = df_doc[['doctor', 'code', 'unit', 'spec']]
    vec_docname = np.frompyfunc(split_docname, 1,3)
    df_doc['name.last'], df_doc['name.first'], df_doc['name.middle'] = \
        vec_docname(df_doc['doctor'])
    df_doc = df_doc[['code', 'unit', 'spec', 'name.last', 'name.first', 'name.middle']]
    df_doc.rename(index=str, columns={"code": "organization.reestr", 
                                    "unit": "squad", 
                                    "spec": "specialty"}, inplace=True)
    df_doc['organization.reestr'] = df_doc['organization.reestr'].astype(str)
    filter_find = df_doc.to_dict('records')
    #df['doctor.id'] = np.nan
    #ds = df['doctor.id']
    df['code'] = df['code'].astype(str)
    #import ipdb; ipdb.set_trace()
    df_result = await do_find_doctors(db, filter_find)
    return df.merge(df_result, how='outer')

async def insert_records(db, df, k):
    """
      подготовка данных и добавление записей расписания в бд
      k - множитель, поскольку записей за раз пытаемся удалить
    """
    print('***готовим данные для добавления в БД {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df['birthday'] = df['birthday'].astype(np.datetime64)
    vec_patname = np.frompyfunc(convert_patname, 3, 1)
    df['name'] = vec_patname(df['sname'], df['fname'], df['lname'])
    df.rename(index=str, columns={'kontakt': 'phone', 
                                'doctor': 'doctor.name', 
                                'birthday': 'birdthDate'}, inplace=True)
    vec_prepare_doctor_data = np.frompyfunc(prepare_doctor_data, 2, 2)
    df['doctor'], df['queue'] = vec_prepare_doctor_data(df['doctor.name'], df['doctor.id'])
    df = df[['date', 'phone', 'record', 'address', 'birdthDate', 'name', 'doctor', 'queue']]
    data_records = df.to_dict('records')
    await do_insert_records(db, data_records, k)
    return df


async def consumer(path_to_file):
    """
      предполагается, что обработчик работает с уже загруженным и сохраненным файлом
    """
    k = 100000 #для монги кол-во удалений вставок за раз
    client = motor.motor_asyncio.AsyncIOMotorClient(DB.get('url', 'localhost'), DB.get('port', 27017))
    db = client[DB['name']]
    print('***загружаем датасет {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df = pd.read_excel(path_to_file, sheetname='Sheet1')
    df = await prepare_doctor_id(db, df)
    df.dropna(subset=['doctor.id'], inplace=True)
    df = await delete_records(db, df, k)
    df = await insert_records(db, df, k)
    pprint.pprint(df.shape)
    pprint.pprint(df.head())
    client.close()
    print('***конец {}'.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))


if __name__ == '__main__':
    PATH = 'now_rec_list.xlsx'
    loop = asyncio.get_event_loop()
    loop.run_until_complete(consumer(PATH))

