"""
  набор методов, обеспечивающих обработку данных, для загрузки изменений в БД,
  практически все, что касается работы с pandas.DataFrame собрано здесь
  !ВАЖНО в текущей версии все ошибки, возвращаемые в scheduler должны быть на
  латинице связано с тем, что smtplib ругается на кирилицу, пока эта проблема 
  не решена
"""

import os
import asyncio 
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import numpy as np
from bson.objectid import ObjectId
import datetime
import time as TIME
import json
import random
import re
import pickle

def check_for_records(data):
    """
      data (в датафрейме это поле days) - массив со словарем, всловаре должны 
      быть ключ slots с непустым значением в этом случае возвращает два поля 
      slots и date, в противном два np.nan
    """
    if data:
        data = data[0]
        date = data.get('date', None)
        slots = data.get('slots', None)
        return slots, date
    else:
        return np.nan, np.nan

def split_intervals(interval):
    """
      ожидается словарь с двумя ключами - begin и end
    """
    return interval.get('begin', 0), interval.get('end', 0)

def split_docname(line, n656):
    if n656 == "Помещение":
        return {"name": line}
    fio = ['last', 'first', 'middle']
    try:
        res = dict(zip(fio, line.split(' ', 2)))
    except Exception as e:
        print("снова ошибка с раздилением на ФИО")
    return {"name.{}".format(k): v for k, v in res.items()}

def search_query_for_doctors(docname, squad, org_reestr, specialty, doc_fullname):
    if isinstance(squad, str) and len(squad.split("_")) > 1:
        sq_var1 = squad.replace("_", " ")
    else:
        sq_var1 = squad

    if isinstance(squad, str) and len(squad.split(" ")) > 1:
        sq_var2 = squad.replace(" ", "_")
    else:
        sq_var2 = squad
    """
    query = {
        '$or': [docname,
            {'name': doc_fullname},
            {'squad': squad}, {'squad': sq_var1}, {'squad': sq_var2}
        ],
        'organization.reestr': org_reestr,
        'specialty': specialty
    }
    """
    if isinstance(specialty, str) and len(specialty.split("_")) > 1:
        spec_var1 = specialty.replace("_", " ")
    else:
        spec_var1 = specialty

    if isinstance(specialty, str) and len(specialty.split(" ")) > 1:
        spec_var2 = specialty.replace(" ", "_")
    else:
        spec_var2 = specialty

    query = {
        '$and': [
            {
                '$or': [docname, {'name': doc_fullname}]
            },
            {
                '$or': [
                    {'squad': squad}, {'squad': sq_var1}, {'squad': sq_var2}
                ]
            },
            {
                '$or': [
                    {'specialty': squad},
                    {'specialty': spec_var1},
                    {'specialty': spec_var2},
                ]
            },
        ],
        'organization.reestr': org_reestr,
    }
    return query
    query = {'$or': [docname, {'name': doc_fullname}]}

    query.update({
        '$or': [{'squad': squad}, {'squad': sq_var1}, {'squad': sq_var2}],
        'organization.reestr': org_reestr,
        'specialty': specialty
    })
    return query

def split_patient_info(data):
    """
      получаем, обрабатываем и разбиваем на три поля данные о пациенте
    """
    fio = data.get('fio', '').split('_')
    for ind in range(len(fio)):
        try:
            fio[ind] = fio[ind] if ind == 0 else f'{fio[ind][0]}.'
        except:
            fio
        
    phone = data.get('@pT', '')
    birth = data.get('pI', '')
    qms_id = data.get('pB', '')
    try:
        birth = datetime.datetime.strptime(str(birth), '%Y%m%d')
    except:
        #ставим текущую
        birth = datetime.datetime(2018, 9, 30, 16, 47, 50, 524754)
    return ' '.join(fio), phone, birth, qms_id

def get_fullName(data):
    fio = data.get('fio', '').split('_')
    keys = ("fullName.last", "fullName.first", "fullName.middle")
    result = dict(zip(keys, fio))
    return (result.get("fullName.last", None), 
            result.get("fullName.first", None),
            result.get("fullName.middle", None))

def calc_rest(intervals, time_interval):
    """
      расчет попадает ли ремя записи к врачам в диапазон перерыва этого врача
    """
    try:
        for interval in intervals:
            if (time_interval >= interval[0]) and (time_interval < interval[1]):
                return True
    except:
        return False
    else:
        return False

def get_time_record(date, time):
    try:
        full_date = datetime.datetime.strptime(f"{date} {time}", '%Y%m%d %H:%M')
        utc_ts = TIME.mktime(full_date.utctimetuple())
        dt = datetime.datetime.utcfromtimestamp(utc_ts)
    except:
        return np.nan
    else:
        return dt

def filling_doctor_for_rec(doc_id, doc_name):
    return {'id': doc_id, 'name': doc_name}

def filling_pat_fullname(last, first, middle):
    return {'last': last, 'first': first, 'middle': middle}

async def get_dataframes_org(data):
    """
      обертка для асинхронных методов
      data - хранит набор коллекций из mongoDB, задача метода преобразовать 
      в датафрейм
    """
    if type(data) == list:
        df = pd.DataFrame.from_records(data)
        return df
    return pandas.DataFrame()

def view_only_by_filter_false(filter_field):
    if isinstance(filter_field, dict):
        filter_field.update(viewOnly=False)
    return filter_field

async def pretreatment_scheduler(df, viewOnly=None):
    """
      предобработка датафрейма с расписанием - переименовываем поля, привеодим
      формат строк к нужному
    """
    try:
        df['pAz'] = df['pAz'].str.replace('_', ' ')
        df['puR'] = df['puR'].str.replace('_', ' ')
    except KeyError:
        return None, 'KeyError: field "pAz" or field "puR" not found'
    else:
        df.rename(index=str, columns={
            'pAz': 'doctor_fullname',
            'RSpID': 'squad',
            'pMO': 'cabinet',
            'puR': 'specialty'
        }, inplace=True)
        #import ipdb; ipdb.set_trace()
        vec_split_docname = np.frompyfunc(split_docname, 2, 1)
        df.dropna(subset=["doctor_fullname"], inplace=True)
        df['doc.name'] = vec_split_docname(df['doctor_fullname'], df["n656"])
        #создадим отдельное поле данными для фильтра mongo
        vec_search_query = np.frompyfunc(search_query_for_doctors, 5, 1)
        df['filter_find'] = vec_search_query(
            df['doc.name'], 
            df['squad'],
            df['reestr'],
            df['specialty'],
            df['doctor_fullname']
        )
        if viewOnly is not None:
            # добавляем в фильтр еще и это поле
            if viewOnly == False:
                vec_view_only = np.frompyfunc(view_only_by_filter_false, 1, 1)
                df['filter_find'] = vec_view_only(df['filter_find'])
        return df, None

async def to_filter_input_data(df_org, df, qms_id):
    """
      навешиваем фильтры на полученную сетку расписания, чтобы отсечь ненужные
      данные
      df_orf - датафрейм с данными по орагнизациям из БД
      df - датафрейм полученный из запроса в qMS
      возвращает  обработанный датафрейм и False либо None и ошибку
    """
    try:
        squads = df_org[df_org['qms_id']==qms_id].iloc[0]['squads']
        squads_ = [item.replace(" ", "_") for item in squads]
        squads.extend(squads_)
        reestr_code = df_org[df_org['qms_id']==qms_id].iloc[0]['reestr']
    except IndexError:
        return None, 'IndexError: specified value "qms_id" or field "reestr" not_found'
    except KeyError:
        return None, 'KeyError: field "squads" or field "reestr" not found'
    else:
        df['RSpID'] = df['RSpID'].apply(lambda x: str(x))
        df = df[df['RSpID'].isin(squads)]
    #теперь уберем те записи, в которых нет пациентов на запись
    days_to_slots_date = np.frompyfunc(check_for_records, 1, 2)
    try:
        df['slots'], df['date'] = days_to_slots_date(df['days'])
    except KeyError:
        return None, 'KeyError: field "days" not found'
    else:
        df.drop(columns=['days'], inplace=True)
        df.dropna(subset=['slots', 'date'], inplace=True)
        df['reestr'] = reestr_code
    return df, None

async def rest_to_dataframe(rest):
    """
      преобразуем массив словарей в датафрейм, поле users (там словари)
      разбиваем на отдельные строки
    """
    df = pd.DataFrame.from_records(rest)
    try:
        #import ipdb; ipdb.set_trace()
        df.dropna(subset=['users'], inplace=True)
        vec_split = np.frompyfunc(split_intervals, 1, 2)
        df['interval_begin'], df['interval_end'] = vec_split(df['interval'])
        if 'publishers' in df.columns.tolist():
            df.drop(columns=['interval', '_id', 'squads', 'publishers'], inplace=True)
        else:
            df.drop(columns=['interval', '_id', 'squads'], inplace=True)
        if 'begin' in df.columns.tolist():
            df.drop(columns=['begin'], inplace=True)
        if 'end' in df.columns.tolist():
            df.drop(columns=['end'], inplace=True)
        columns = df.drop('users', axis=1).columns.tolist()
        count_field = len(columns)
        df_temp= df.groupby(columns).users.\
            apply(lambda x: pd.DataFrame(x.values[0])).reset_index().\
            drop(f'level_{count_field}', axis = 1)
        columns.append('users')
        df_temp.columns = columns
    except Exception as e:
        #import ipdb; ipdb.set_trace()
        return None, f'failed to process data from the receptionRest collection: {e}'
    else:
        return df_temp, None

#TODO: login в теории, можно убрать и НУЖНО!
async def posttreatment_scheduler(df, doctors_from_db, df_rest):
    """
      подготовка данных для внесения в базу данных, doctors_from_db - records
      откуда будем брать id и логин доктора
    """
    df_save = df.copy()
    df_from_db = pd.DataFrame.from_records(doctors_from_db)
    #оставляем только нужные для работы поля
    #import ipdb;ipdb.set_trace()
    if 'name' not in df_from_db.columns:
        return None, 'KeyError: name'
    df_from_db["deleted"] = df_from_db["deleted"].fillna(False)
    df_from_db = df_from_db[df_from_db["deleted"]==False]
    df_from_db['doctor_fullname'] = df_from_db['name'].map(lambda x: \
        f"{x.get('last', '')} {x.get('first', '')} {x.get('middle', '')}" if isinstance(x, dict) else x)
    df_from_db['reestr'] = df_from_db['organization'].\
        map(lambda x: f"{x.get('reestr', '')}")
    if "login" not in df_from_db.columns:
        df_from_db["login"] = "undefined"
    df_from_db = df_from_db[['_id', 'doctor_fullname', 
        'reestr', 'specialty', 'squad', 'login']]
    #в расписании тоже оставляем только нужные для работы поля
    #import ipdb;ipdb.set_trace()
    if 'cabinet' not in df.columns:
        df['cabinet'] = 0
    df = df[['squad', 'doctor_fullname', 'specialty', 'slots',
       'date', 'reestr', 'cabinet']]
    squad = df_from_db["squad"]
    squad_ = df_from_db["squad"].map(lambda x: x.replace(" ", "_") if isinstance(x, str) else x)
    spec = df_from_db["specialty"]
    spec_ = df_from_db["specialty"].map(lambda x: x.replace("_", " ") if isinstance(x, str) else x)
    df1 = df.merge(df_from_db, on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how="inner")
    df_from_db["squad"] = squad_
    df2 = df.merge(df_from_db, on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how="inner")
    df_from_db["squad"] = squad
    df_from_db["specialty"] = spec_
    df3 = df.merge(df_from_db, on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how="inner")
    df_from_db["squad"] = squad_
    df4 = df.merge(df_from_db, on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how="inner")
    #import ipdb;ipdb.set_trace()
    """
    df1 = df.merge(df_from_db, on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how="inner")
    df_from_db["squad"] = df_from_db["squad"].map(lambda x: x.replace(" ", "_") if isinstance(x, str) else x)
    df2 = df.merge(df_from_db, on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how="inner")
    """
    df = pd.concat([df1,df2,df3,df4])
    df.drop_duplicates(subset=['squad', 'doctor_fullname', 'specialty', 'reestr'], inplace=True)
    """
    df = df.merge(df_from_db, 
        on=['squad', 'doctor_fullname', 'specialty', 'reestr'], how='outer')
    """
    #import ipdb;ipdb.set_trace()
    df.dropna(subset=['_id'], inplace=True)
    df["cabinet"] = df["cabinet"].fillna("undefined")
    df.rename(index=str, columns={'_id': 'doctor.id'}, inplace=True)
    #штука нужна для создания уникальных индексов при вызове groupby чтобы 
    #записи, в которых отличается только slots не пропадали
    df['dop_id'] = df.index 
    #раскладываем slots на строки
    columns = df.drop('slots', axis=1).columns.tolist()
    count_field = len(columns)
    #import ipdb;ipdb.set_trace()
    df["login"] = df["login"].fillna("undefined")
    df= df.groupby(columns).slots.apply(lambda x: pd.DataFrame(x.values[0])).\
        reset_index().drop(f'level_{count_field}', axis = 1)
    #import ipdb;ipdb.set_trace()
    try:
        if 'patients' in df.columns:
            df.dropna(subset=['patients'], inplace=True)
            df['patients'] = df['patients'].map(lambda x: x[0])
            vec_split = np.frompyfunc(split_patient_info, 1, 4)
            df['name'], df['phone'], df['birdthDate'], df['qms_id'] = vec_split(df['patients'])
            vec_fullName = np.frompyfunc(get_fullName, 1, 3)
            df['fullName.last'], df['fullName.first'], df['fullName.middle'] = vec_fullName(df['patients'])
            df.drop(columns=['pars', 'patients'], inplace=True)
        else:
            df['name'] = "undefined"
            df['phone'] = "undefined"
            df['birdthDate'] = "undefined"
            df['qms_id'] = "undefined"
        df['time'] = df['time'].map(lambda x: x.split('-')[0])
        df['time_interval'] = df['time'].\
            map(lambda x: int(x.split(':')[0]) * 60 + int(x.split(':')[1]))
        #перегруппировываем df_rest
        df_rest = df_rest.groupby('users')['interval_begin', 'interval_end'].\
            apply(lambda x: x.values.tolist()).reset_index()
        df_rest.rename(index=str, columns={'users': 'login'}, inplace=True)
        #import ipdb;ipdb.set_trace()
        df = df.merge(df_rest, on='login', how='outer')
        df.rename(index=str, columns={0: 'intervals'}, inplace=True)
        vect_calc_rest = np.frompyfunc(calc_rest, 2, 1)
        #import ipdb;ipdb.set_trace()
        df['rest'] = vect_calc_rest(df['intervals'], df['time_interval'])
        df.dropna(subset=['doctor_fullname', 'name'], inplace= True)
        df['date'] = df['date'].astype(np.int)
        df['date'] = df['date'].astype(np.str)
        vect_time_record = np.frompyfunc(get_time_record, 2, 1)
        df['record'] = vect_time_record(df['date'], df['time'])
        df.dropna(subset=['record',], inplace= True)
        d_now = datetime.datetime.today()
        df['date'] = d_now.strftime('%d.%m.%Y')
        #import ipdb;ipdb.set_trace()
        if 'fullName.last' not in df.columns:
            df['fullName.last'] = "undefined"
        if 'fullName.first' not in df.columns:
            df['fullName.first'] = "undefined"
        if 'fullName.middle' not in df.columns:
            df['fullName.middle'] = "undefined"
        #убираем все ненужное
        df['organization'] = df['reestr']
        df = df[['organization', 'doctor_fullname', 'date', 'doctor.id', 'name', 'phone', 
            'birdthDate', 'rest', 'record', 'qms_id', 'fullName.last',
            'fullName.first', 'fullName.middle']]
        df['doctor.id'] = df['doctor.id'].astype(np.str)
    except Exception as e:
        print(e)
        return None, e
    else:
        return df, None

async def search_dubles_request(df):
    """
      возвращает строку для поиска дублей в бд
    """
    if df is None:
        return None
    return df[['doctor.id', 'record']].to_dict('records')

async def preparing_schedule_for_recording(df, doubles):
    """
      последняя подготовка данных к записи в бд - создание нужных полей,
      "спасение" reception из doubles, привидение даных в полях к нужному виду 
    """
    if doubles:
        df_doubles = pd.DataFrame.from_records(doubles)
        df_doubles['doctor.id'] = df_doubles['doctor'].\
            map(lambda x: x.get('id', np.nan))
        #убираем лишнее
        df_doubles = df_doubles[['doctor.id', 'name', 'record', 'reception']]
        df = df.merge(df_doubles, on=['doctor.id', 'name', 'record'], how='outer')
        #временно так:, способ гарантирует внесение новых данных, но не 
        #гарантирует update измененных данных
        df = df.drop_duplicates(subset=["name", "record"], keep="first")
        df = df[df['reception'].isna()]
    else:
        df['reception'] = np.nan
    to_remove = df[['doctor.id', 'record']].to_dict('records')
    df['reception'] = df['reception'].map(lambda x: x if type(x)==list else [])
    df['queue'] = df['reception'].map(lambda x: {'type': 'plan'})
    vec_filling_doctor = np.frompyfunc(filling_doctor_for_rec, 2, 1)
    df['doctor'] = vec_filling_doctor(df['doctor.id'], df['doctor_fullname'])
    vec_fullname = np.frompyfunc(filling_pat_fullname, 3, 1)
    df["fullName"] = vec_fullname(
        df['fullName.last'], df['fullName.first'], df['fullName.middle'])
    df['address'] = ''
    df = df[['date', 'doctor', 'record', 'address', 'birdthDate', 'name', 
            'phone', 'queue', 'reception', 'rest', 'qms_id', 'fullName', 'organization']]
    return df.to_dict('records'), to_remove

#-------------------------------------функции для timetable_online
def check_for_records_timetable(data):
    """
      data (в датафрейме это поле days) - массив со словарем, всловаре должны 
      быть ключ slots с непустым значением в этом случае возвращает два поля 
      slots и date, в противном два np.nan
    """
    if data:
        data = data[0]
        date = data.get('date', None)
        slots = data.get('slots', None)
        p_pars = data.get('pars', None)
        #проверка, если в slots.pars пусто, то меняем на p_pars
        if slots:
            for i in range(len(slots)):
                item = slots[i]
                gr = re.findall(r'\w{3}', item['pars'])
                if not gr:
                    slots[i]['pars'] = p_pars
        return slots, date
    else:
        return np.nan, np.nan

def convert_date(date):
    try:
        return datetime.datetime.strftime(date, '%Y%m%d')
    except TypeError:
        return datetime.datetime.strftime(datetime.datetime.today(), '%Y%m%d')

def reverse_convert_date(str_date):
    try:
        return datetime.datetime.strptime(str_date, '%Y%m%d')
    except TypeError:
        print('TYPEERROR============='*8)
        return datetime.datetime.today()

def convert_str_to_date(str_date, str_time):
    """
      на выход два значения datetime начальная и datetime конечное
      (предполагается,что time передается в виде 08:10-08:30_
    """
    time_list = str_time.split('-')
    date_start = f"{str_date}:{time_list[0]}"
    date_end = f"{str_date}:{time_list[1]}"
    date = datetime.datetime.strptime(f"{str_date}", '%Y%m%d')
    date_start = datetime.datetime.strptime(date_start, '%Y%m%d:%H:%M')
    date_end = datetime.datetime.strptime(date_end, '%Y%m%d:%H:%M')
    return {'begin': date_start, 'end': date_end}, date

def search_empty_cells(patients):
    if type(patients) == float:
        return True
    return False

def filling_child_pars(parent, child):
    """
      если в дочернем pars нет слов длинее трех символов, то ставляем 
      родительский pars
    """
    gr = re.findall(r'\w{3}', child)
    if not gr:
        child = parent
    return child

async def get_dates(date_range):
    """
      функция для получения диапазона дат диапазон равен date_range
      начиная с текущего дня
    """
    dates = pd.date_range(pd.datetime.today(), periods=date_range)
    dates = dates.map(convert_date).tolist()
    return dates

async def read_datafile(datafile, organization, date):
    """
      считываем файл с данными, преобразуем в датафрейм, добавляем данные
      из organisation (код реестра) и date, date - строка, формата YYYYMMDD
    """
    try:
        data = json.load(open(datafile))
        #здесь преобразуем данные в Dataframe 
        df = pd.DataFrame.from_records(data['data'])
        error = None
    except Exception as e:
        error = f'ошибка {e}'
        #print(error)
        df = pd.DataFrame()
    else:
        #если исключений не было вызвано, приклеиваем дополнительные поля
        if not df.empty:
            df['date_qms'] = date
            df['date_qms'] = df['date_qms'].astype(str)
            df['date'] = reverse_convert_date(date)
            df['organization'] = organization.get('reestr', 'undefined')
            df_cmp = pd.DataFrame({'n656': organization.get("filter_n656", [])})
            df = df.merge(df_cmp, left_on="n656", right_on="n656")
    finally:
        return df, error
    #return pd.DataFrame({'test': ['12']}), None

def apply_filter_types(pars, types):
    """
      осуществляет фитр по типу записи, если удовлетворяет условию - вернуть
      True, если нет - np.nan
    """
    #pars = pars.lower()
    include = types.get('include', [])
    try:
        include = [item.get('pers', '') for item in include]
    except:
        pass
    exclude = types.get('exclude', [])
    label = False
    out_type= ''
    if include and exclude:
        include = list(set(include)-set(exclude))
        include.sort(reverse=True)
        for rule in include:
            if rule in pars:
                label = True
                out_type = rule
    elif include and not exclude:
        include.sort(reverse=True)
        for rule in include:
            if rule in pars:
                label = True
                out_type = rule
    elif not include and exclude:
        exclude.sort(reverse=True)
        for rule in exclude:
            if rule in pars:
                label = True
                out_type = rule
    else:
        #случай, когда путсы и include и exclude - считаем, что возвращаем
        #все данные
        label = True
    if label:
        return label, out_type
    return np.nan, out_type

async def converted_dataframe(df, types):
    """
      проводим все необходимы преобразования в датафрейме, согласно 
      утвержденному шаблону
      types - разрешенные и запрещенные к показу типы записи
    """
    #df_save = df
    if df.empty:
        return None, 'Dataframe empty'
    #days_to_slots_date = np.frompyfunc(check_for_records_timetable, 1, 2)
    days_to_slots_date = np.frompyfunc(check_for_records_timetable, 1, 2)
    try:
        df['slots'], df['date'] = days_to_slots_date(df['days'])
        df.drop(columns=['days'], inplace=True)
        df['date'] = df['date'].astype(str)
    except Exception as e:
        return None, f'{e}: field "days" not found'
    df['pAz'] = df['pAz'].fillna(value=df['pMO'])
    df['pMO'] = df['pMO'].fillna(value="undefined")
    df = df.dropna(subset=['pAz'])
    columns = df.drop('slots', axis=1).columns.tolist()
    count_field = len(columns)
    df = df.groupby(columns).slots.apply(lambda x: pd.DataFrame(x.values[0])).\
        reset_index().drop(f'level_{count_field}', axis = 1)
    try:
        df.rename(index=str, columns={'pAz': 'doctor', 'puR': 'specialty'}, inplace=True)
        vec_empty_cells = np.frompyfunc(search_empty_cells, 1, 1)
        if 'patients' not in df.columns:
            df['patients'] = np.nan
        df['empty'] = vec_empty_cells(df['patients'])
        df.drop(columns=['patients'], inplace=True)
    except Exception as e:
        return None, f'{e}: field "pAz" or "patients" or "puR" not found'
    #убираем те записи, которые нас не интересуют
    vect_apply_types = np.frompyfunc(apply_filter_types, 2, 2)
    if df.empty:
        return None, 'Dataframe not slots'
    df['waste'], df['out_type'] = vect_apply_types(df['pars'], types)   
    df.dropna(subset=['waste'], inplace=True)
    str_to_date = np.frompyfunc(convert_str_to_date, 2, 2)
    df['record'], df['date'] = str_to_date(df['date'], df['time'])
    #--------
    id_fields = ['doctor', 'RSpID', 'specialty', 'out_type']
    counts = df.groupby(id_fields).size()
    df2 = pd.DataFrame(counts, columns = ['total']).reset_index()
    try:
        df = df.merge(df2, on=id_fields, how='outer')
    except Exception as e:
        return None, f"Снова ошибка с мержем {e}"
    counts = df.groupby(id_fields).empty.sum()
    df2 = pd.DataFrame(counts).reset_index()
    df2.rename(index=str, columns={'empty': 'empty_total'}, inplace=True)
    df = df.merge(df2, on=id_fields, how='outer')
    df['empty_total'] = df['empty_total'].map(lambda x: 0 if x==False else x)
    df['empty_total'] = df['empty_total'].map(lambda x: 1 if x==True else x)
    return df, None

def request_in_mongo_state(organization, doctor, date, specialty, total, empty, out_type, n656):
    date = datetime.datetime.combine(date, datetime.datetime.min.time())
    filt = dict(organization=organization, doctor=doctor, date=date, specialty=specialty, out_type=out_type)
    data = dict(total=total, empty=empty, n656=n656)
    return {'filter': filt, 'data': data}

def request_in_mongo_schedule(organization, doctor, record, specialty, empty, date_qms, out_type):
    filt = {
        'organization': organization,
        'doctor': doctor,
        'record.begin': record.get('begin', None),
        'record.end': record.get('end', None),
        'specialty': specialty,
        'out_type': out_type
    }
    data = dict(date_qms=date_qms, empty=empty)
    return {'filter': filt, 'data': data}

async def data_for_record(df):
    """
      подготавливает данные для записи в бд
      {
            organization: 240000,
            doctor: Иванов,
            record: 
                {
                    begin: date,
                    end: date
                },
            empty: false,
            specialty: dfsdf
        }

        а для агрегированного в формате
        {
            organization: 240000,
            doctor: Иванов,
            date: 18.10.2018,
            total: 24,
            empty: 12,
            specialty: sdsd
        }
    """
    try: 
        id_fields = ['doctor', 'RSpID', 'specialty', 'out_type']
        rec_state = df.drop_duplicates(id_fields)
        rec_state = rec_state[['organization', 'doctor', 'date', 'total', 
            'empty_total', 'specialty', 'pars', 'out_type', 'RSpID', 'n656']]
        rec_state.rename(index=str, columns={'empty_total': 'empty'}, inplace=True)
        #----
        rec_state['date'] = rec_state['date'].map(lambda x: x.date())
        vec_request = np.frompyfunc(request_in_mongo_state, 8, 1)
        rec_state['request'] = vec_request(rec_state['organization'],\
            rec_state['doctor'],rec_state['date'],rec_state['specialty'],\
            rec_state['total'],rec_state['empty'], rec_state['out_type'],\
            rec_state['n656'])
        rec_state_requests = rec_state['request'].values.tolist()
        #----
        rec_scheduler = df[['organization', 'doctor', 'record', 'empty', 'specialty', 'date_qms', 'pars', 'out_type', 'RSpID']]
        #----
        vec_request = np.frompyfunc(request_in_mongo_schedule, 7, 1)
        rec_scheduler['request'] = vec_request(rec_scheduler['organization'],\
            rec_scheduler['doctor'],rec_scheduler['record'],rec_scheduler['specialty'],\
            rec_scheduler['empty'],rec_scheduler['date_qms'], rec_scheduler['out_type'])
        rec_schedule_requests = rec_scheduler['request'].values.tolist()
        #rec_schedule_requests = "ewhjekwe"
        #----
        #rec_state_requests = await data_state_for_fccs('tempfiles/fc20181130.json')
        #rec_schedule_requests = 1
    except Exception as e:
        print(f'{e}')
        return None, None
    else:
        #
        return rec_state_requests, rec_schedule_requests
        return rec_state.to_dict('records'), rec_scheduler.to_dict('records')

async def data_state_for_fccs(path):
    data = json.load(open(path))
    out = []
    for item in data:
        df = pd.DataFrame.from_records(item['data'])
        #df['organization'] = 'fccs'
        df['organization'] = '240230'
        columns = df.drop('days', axis=1).columns.tolist()
        count_field = len(columns)
        df = df.groupby(columns).days.apply(lambda x: pd.DataFrame(x.values[0])).reset_index().drop(f'level_{count_field}', axis = 1)
        df['date_qms'] = df['date']
        df['date_qms'] = df['date_qms'].astype(str)
        df.drop(columns=['pars'], inplace=True)
        df['pMO'] = 'undefined'
        df = df.dropna(subset=['pAz'])
        columns = df.drop('slots', axis=1).columns.tolist()
        count_field = len(columns)
        df = df.groupby(columns).slots.apply(lambda x: pd.DataFrame(x.values[0])).\
            reset_index().drop(f'level_{count_field}', axis = 1)
        df.rename(index=str, columns={'pAz': 'doctor', 'puR': 'specialty'}, inplace=True)
        def search_empty_cells(patients): return False if patients else True
        vec_empty_cells = np.frompyfunc(search_empty_cells, 1, 1)
        df['empty'] = vec_empty_cells(df['patients'])
        df.drop(columns=['patients'], inplace=True)
        df['out_type'] = df['pars']
        df['time'] = df['time'].apply(lambda x: "00:00-00:20" if x=="" else x)
        str_to_date = np.frompyfunc(convert_str_to_date, 2, 2)
        df['record'], df['date'] = str_to_date(df['date'], df['time'])
        id_fields = ['doctor', 'RSpID', 'specialty', 'out_type']
        counts = df.groupby(id_fields).size()
        df2 = pd.DataFrame(counts, columns = ['total']).reset_index()
        try:
            df = df.merge(df2, on=id_fields, how='outer')
        except Exception as e:
            return None, f"Снова ошибка с мержем {e}"
        
        counts = df.groupby(id_fields).empty.sum()
        df2 = pd.DataFrame(counts).reset_index()
        df2.rename(index=str, columns={'empty': 'empty_total'}, inplace=True)
        df = df.merge(df2, on=id_fields, how='outer')
        df['empty_total'] = df['empty_total'].map(lambda x: 0 if x==False else x)
        df['empty_total'] = df['empty_total'].map(lambda x: 1 if x==True else x)
        id_fields = ['doctor', 'RSpID', 'specialty', 'out_type', 'date_qms']
        rec_state = df.drop_duplicates(id_fields)
        rec_state = rec_state[['n656', 'organization', 'doctor', 'date', 'total', 'empty_total', 'specialty', 'pars', 'out_type', 'RSpID']]
        rec_state.rename(index=str, columns={'empty_total': 'empty'}, inplace=True)
        #----
        rec_state['date'] = rec_state['date'].map(lambda x: x.date())
        vec_request = np.frompyfunc(request_in_mongo_state, 8, 1)
        rec_state['request'] = vec_request(rec_state['organization'],\
            rec_state['doctor'],rec_state['date'],rec_state['specialty'],\
            rec_state['total'],rec_state['empty'], rec_state['out_type'])
        out.extend(rec_state['request'].values.tolist())
    return out


async def compare_cache_with_df(state, schedule, state_cache, schedule_cache):
    """
      сравнение имеющихся в кэше данных иданных полученных от кумсы
    """
    rec1 = state[:2]
    rec2 = state[1:3]
    df1 = pd.DataFrame(rec1)
    df2 = pd.DataFrame(rec2)
    df1['str_data'] = df1['data'].map(lambda x: pickle.dumps(x))
    df1['str_filter'] = df1['filter'].map(lambda x: pickle.dumps(x))
    df2['str_data'] = df1['data'].map(lambda x: pickle.dumps(x))
    df2['str_filter'] = df1['filter'].map(lambda x: pickle.dumps(x))

    #import ipdb; ipdb.set_trace()
    if state_cache and schedule_cache:
        df_st = pd.DataFrame(state)
        df_st_cache = pd.DataFrame(schedule_cache)
    return state, schedule
    
    return df[df['doctor'].isin(doctors)]



async def records_to_dataframe(records):
    """
      преобразование записей из бд, полученных фильтром в dataframe
    """ 
    return pd.DataFrame(records)



