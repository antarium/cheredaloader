"""
  файл, описывающий настройки сервера и данных для подключения к БД
"""
import logging
import os

DATABASE = {
    'default':{
        'url': '10.80.99.17',
        #'url': '127.0.0.1',
        'port': 27017,
        'name': 'WaitingList'
    }
}

COLLECTIONS = {
    'doctors': 'doctors',
    'org': 'organizations',
    'records': 'patientsList'
}

LOG_PATH = 'logs'

TEMP_PATH = 'tempfiles'
URL_QMS_REQUEST = 'http://10.61.103.50:5003/csp/qms/rest/schedule'
QMS_CHUNK_SIZE = 10 #размер блоков, получаемы при запросе расписания qms

scheduler_logger = logging.getLogger('scheduler')
scheduler_logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(os.path.join(LOG_PATH, 'cheredaloader.log'))
formatter = logging.Formatter('%(asctime)s - %(name)s: %(filename)s[LINE:%(lineno)d]# %(levelname)-8s  %(message)s')
fh.setFormatter(formatter)
scheduler_logger.addHandler(fh)


MNRG_HOST = '127.0.0.1'
MNGR_PORT = 54111

SCHEDULER_CHECK_PAUSE = 10 #раз в это время планировщик будет проверять state

#если True, то расписание будет считываться из локальных файлов
DEBUG_LOCAL = False 

#----------Настройки для timetable_online

DATABASE_TT = {
    'default':{
        'url': '10.80.99.17',
        #'url': '127.0.0.1',
        'port': 27017,
        #'name': 'WaitingList',
        'name': 'ScheduleMonitor'
    }
}

COLLECTIONS_TT = {
    'state': 'State', #аггрегированные данные
    'org': 'Organizations',
    'schedule': 'Schedule' #индивидуальные данные
}

DEBUG_TT = False 

TIME_UPDATE_TT = 1800

REDIS = {
    'url': '10.80.0.250',
    'port': 6379,
    'db': 0  
}

#ДЛЯ ПЕРВОЙ КРАЕВОЙ ВРЕМЕННО (ПОТОМ БУДЕМ ИЗ БАЗЫ БРАТЬ)
REQUEST_TEMP = {"qqc235":"vAE","date":20181130,"qqc244list":"vAEdAAnAPAC vAEdAAnAVAC vAEdAAnAXAU vAEdAAnAXAR vAEdAAnAXAV vAEdAAnAXAN vAEdAAnAXAM vAEdAAnAXAO vAEdAAnCZAJ vAEdAAnAIAQ vAEdAAnAXAQ vAEdAAnDXAC vAEdAAnDXAF vAEdAAnAXAP vAEdAAnDXAB vAEdAAnAmAF vAEdAAnDXAE vAEdAAnACAE vAEdAAnDwAC vAEdAAnAAAa vAEdAAnAAAc vAEdAAnAAAX vAEdAAnAAAY vAEdAAnAAAe vAEdAAnAsAj vAEdAAnAIAO vAEdAAnAIAI vAEdAAnEaAC vAEdAAnAAAl vAEdAAnGmAI vAEdAAnAGAC vAEdAAnAGAB vAEdAAnEeAC vAEdAAnAhAJ vAEdAAnAmAD vAEdAAnExAC vAEdAAnCHAJ vAEdAAnCvAD vAEdAAnDXAH vAEdAAnAUAF vAEdAAnAGAA vAEdAAnEcAA vAEdAAnFCAA vAEdAAnGmAK vAEdAAnAUAD vAEdAAnAOAF vAEdAAnExAA vAEdAAnGmAH vAEdAAnCHAH vAEdAAnCHAG vAEdAAnAdAH vAEdAAnCHAB vAEdAAnEgAA vAEdAAnAeAC vAEdAAnCvAC vAEdAAnDXAI vAEdAAnCHAC vAEdAAnAJAE vAEdAAnAHAI vAEdAAnEwAB vAEdAAnCHAD vAEdAAnGmAL vAEdAAnCHAE vAEdAAnAUAE vAEdAAnEVAB vAEdAAnAJAG vAEdAAnGmAJ vAEdAAnAEAB vAEdAAnAIAR vAEdAAnExAB vAEdAAnFTAA vAEdAAnFVAA vAEdAAnCHAF vAEdAAnAOAH vAEdAAnAOAG vAEdAAnAOAE vAEdAAnAIAH vAEdAAnAHAH vAEdAAnAIAJ vAEdAAnEaAD vAEdAAnEhAE vAEdAAnEaAE vAEdAAnAfAJ vAEdAAnGpAJ vAEdAAnFMAA vAEdAAnALAC vAEdAAnAfAL vAEdAAnGpAL vAEdAAnEhAF vAEdAAnAVAF vAEdAAnEhAC vAEdAAnEhAB vAEdAAnEhAA vAEdAAnApAK vAEdAAnABAC vAEdAAnEyAE vAEdAAnCHAI vAEdAAnFqAA vAEdAAnGpAM vAEdAAnFtAA vAEdAAnFtAB vAEdAAnAVAG vAEdAAnFpAA vAEdAAnAUAC vAEdAAnEhAD vAEdAAnApAJ vAEdAAnEaAF vAEdAAnEaAA vAEdAAnAHAJ vAEdAAnEaAG vAEdAAnEyAA vAEdAAnEyAF vAEdAAnAHAK vAEdAAnEaAI vAEdAAnEyAD vAEdAAnEyAB vAEdAAnDBAB vAEdAAnALAB vAEdAAnFyAD vAEdAAnEtAA vAEdAAnEtAC vAEdAAnGoAA vAEdAAnAIAG vAEdAAnEtAB"}

URL_TEMP = "http://10.133.0.3:57772/csp/user/rest/schedule"



