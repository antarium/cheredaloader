"""
  модуль (временный) агрегирующий все методы, кроме вебсервера для загрузки
  расписания, реализуется весь алгоритм
"""
 
import motor.motor_asyncio
import settings as conf
import asyncio
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import numpy as np
import datetime
import time as TIME
import pprint


DB = conf.DATABASE['default']
COLLECTIONS = {
    'doctors': 'doctors',
    'org': 'organizations',
    'records': 'patientsList'
}
client = motor.motor_asyncio.AsyncIOMotorClient(DB.get('url', 'localhost'), DB.get('port', 27017))
db = client[DB['name']]

def split_docname(line):
    fio = ['last', 'first', 'middle']
    res = dict(zip(fio, line.split(' ')))
    return {"name.{}".format(k): v for k, v in res.items()}

async def do_find(filter, df_test, ds):
    collection = db.doctors
    async for doc in collection.find({'$or': filter}):
        doc_id = str(doc['_id'])
        name = ' '.join([doc['name']['last'], doc['name']['first'], doc['name']['middle']])
        indexes = df_test[(df_test.code==doc['organization']['reestr'])&(df_test.unit==doc['squad'])&(df_test.doctor==name)&(df_test.spec==doc['specialty'])].index
        ds[indexes.tolist()] = doc_id

def convert_datetime(date, time):
    time = time.split('-')[0]
    full_date = datetime.datetime.strptime("{}-{}".format(date, time), '%d.%m.%y-%H:%M')
    utc_ts = TIME.mktime(full_date.utctimetuple())
    dt = datetime.datetime.utcfromtimestamp(utc_ts)

vec_convert = np.frompyfunc(convert_datetime, 2, 1)

async def do_delete_many(for_delete):
    coll = db.patientsList
    n = await coll.count_documents({})
    print('%s documents before calling delete_many()' % n)
    result = await coll.delete_many({'$or': for_delete})
    print('%s documents after' % (await coll.count_documents({})))

def convert_patname(middle, first, last):
    first = "{}.".format(first[0]) if first else ''
    middle = "{}.".format(middle[0]) if middle else ''
    return "{} {}{}".format(last.capitalize(), first.capitalize(), middle.capitalize())

vec_patname = np.frompyfunc(convert_patname, 3, 1)

async def do_insert_many(data):
    collection = db.patientsList
    result = await collection.insert_many(data)
    print('inserted %d docs' % (len(result.inserted_ids),))


#if __name__ == 'main':
def run():
    loop = asyncio.get_event_loop()
    print('=================загружаем датасет {}================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df = pd.read_excel('now_rec_list.xlsx', sheetname='Sheet1')
    df_test = df.iloc[100:300]
    print('=================начинаем обработку {}================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df_doc = df_test.drop_duplicates(['doctor', 'code', 'unit', 'spec'])
    df_doc = df_doc[['doctor', 'code', 'unit', 'spec']]
    fio = ['last', 'first', 'middle']
    df_doc['name'] = df_doc['doctor'].apply(split_docname)
    ds_name = df_doc['name']
    df_doc = df_doc[['code', 'unit', 'spec']]
    df_doc.rename(index=str, columns={"code": "organization.reestr", "unit": "squad", "spec": "specialty"}, inplace=True)
    df_doc['organization.reestr'] = df_doc['organization.reestr'].astype(str)
    print('=================получаем данные врачей {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    filter = [{**df_doc.loc[ind].to_dict(), **ds_name.loc[int(ind)]} for ind in df_doc.index]
    df_test['doctor.id'] = np.nan
    ds = df_test['doctor.id']
    df_test['code'] = df_test['code'].astype(str)
    loop.run_until_complete(do_find(filter, df_test, ds))
    df_test.dropna(subset=['doctor.id'], inplace=True)
    df_test['record'] = vec_convert(df_test['date'], df_test['time'])
    df_test['date'] = datetime.datetime.strftime(datetime.datetime.today(), '%d.%m.%y')
    print('=================удаляем дублирующие данные {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    for_delete = [df_test[['doctor.id', 'record']].loc[ind].to_dict() for ind in df_test.index]
    loop.run_until_complete(do_delete_many(for_delete))
    print('=================записываем данные {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df_test['birthday'] = df_test['birthday'].astype(np.datetime64)
    df_test['name'] = vec_patname(df_test['sname'], df_test['fname'], df_test['lname'])
    df_test.rename(index=str, columns={'kontakt': 'phone', 'doctor': 'doctor.name', 'birthday': 'birdthDate'}, inplace=True)
    df_doc = df_test[['doctor.name', 'doctor.id']]
    df_doc.rename(index=str, columns={'doctor.name': 'name', 'doctor.id': 'id'}, inplace=True)
    df_test = df_test[['date', 'phone', 'record', 'address', 'birdthDate', 'name']]
    data_records = [{**df_test.loc[ind].to_dict(), **{'doctor': df_doc.loc[ind].to_dict()}, **{'queue': {'type': 'plan'}}} for ind in df_test.index]
    loop.run_until_complete(do_insert_many(data_records))
    print('=================готово {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))

def run2():
    loop = asyncio.get_event_loop()
    print('=================загружаем датасет {}================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df = pd.read_excel('now_rec_list.xlsx', sheetname='Sheet1')
    print('=================начинаем обработку {}================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df_doc = df.drop_duplicates(['doctor', 'code', 'unit', 'spec'])
    df_doc = df_doc[['doctor', 'code', 'unit', 'spec']]
    fio = ['last', 'first', 'middle']
    df_doc['name'] = df_doc['doctor'].apply(split_docname)
    ds_name = df_doc['name']
    df_doc = df_doc[['code', 'unit', 'spec']]
    df_doc.rename(index=str, columns={"code": "organization.reestr", "unit": "squad", "spec": "specialty"}, inplace=True)
    df_doc['organization.reestr'] = df_doc['organization.reestr'].astype(str)
    print('=================получаем данные врачей {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    filter = [{**df_doc.loc[ind].to_dict(), **ds_name.loc[int(ind)]} for ind in df_doc.index]
    df['doctor.id'] = np.nan
    ds = df['doctor.id']
    df['code'] = df['code'].astype(str)
    loop.run_until_complete(do_find(filter, df, ds))
    df.dropna(subset=['doctor.id'], inplace=True)
    df['record'] = vec_convert(df['date'], df['time'])
    df['date'] = datetime.datetime.strftime(datetime.datetime.today(), '%d.%m.%y')
    print('=================удаляем дублирующие данные {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    for_delete = [df[['doctor.id', 'record']].loc[ind].to_dict() for ind in df.index]
    loop.run_until_complete(do_delete_many(for_delete))
    print('=================записываем данные {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))
    df['birthday'] = df['birthday'].astype(np.datetime64)
    df['name'] = vec_patname(df['sname'], df['fname'], df['lname'])
    df.rename(index=str, columns={'kontakt': 'phone', 'doctor': 'doctor.name', 'birthday': 'birdthDate'}, inplace=True)
    df_doc = df[['doctor.name', 'doctor.id']]
    df_doc.rename(index=str, columns={'doctor.name': 'name', 'doctor.id': 'id'}, inplace=True)
    df = df[['date', 'phone', 'record', 'address', 'birdthDate', 'name']]
    data_records = [{**df.loc[ind].to_dict(), **{'doctor': df_doc.loc[ind].to_dict()}, **{'queue': {'type': 'plan'}}} for ind in df.index]
    loop.run_until_complete(do_insert_many(data_records))
    print('=================готово {}==================='.format(
        datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')))





