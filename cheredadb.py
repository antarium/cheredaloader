"""
  классы, обеспечивающие работу с основной БД череды mongoDB
"""
import motor.motor_asyncio
import settings as conf
import asyncio
import logging
from prepare import *


class CheredaDB:
    """
      реализует работу с бд в mongo
    """
    DB = conf.DATABASE['default']
    CLIENT_NAME_FOR_LOG = 'client'
    COLLECTIONS = {
        'doctors': 'doctors',
        'org': 'organizations',
        'records': 'patientsList',
        'rest': 'receptionRest'
    }
    db_log = logging.getLogger("scheduler.db")

    def __init__(self):
        self.db_log.info(f'mongo {self.CLIENT_NAME_FOR_LOG} init')
        """
        self.client = motor.motor_asyncio.AsyncIOMotorClient(
            self.DB.get('url', 'localhost'), self.DB.get('port', 27017))
        """

    async def __aenter__(self):
        """
          Открываем соединение
        """
        self.client = motor.motor_asyncio.AsyncIOMotorClient(
            self.DB.get('url', 'localhost'), self.DB.get('port', 27017))
        self.db = self.client[self.DB['name']]
        return self
        #return self.client

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        """
          Закрываем подключение.
        """
        self.db_log.info(f'mongo {self.CLIENT_NAME_FOR_LOG} close')
        self.client.close()
        if exc_val:
            raise

    async def by_filtering(self, collect, filt=None, out=None):
        """
          делает выборку find()
          collect - коллекция из которой делать выборку 
          filt - условия фильтра, если пусто, то без фильтра
          out - ключ, по которому выводить информацию, если 
          None, то выводить все
        """
        collection = self.db[collect]
        cursor = collection.find(filt)
        if out:
            result = [doc[out] async for doc in cursor if doc.get(out, None)]
        else:
            result = [doc async for doc in cursor]
        return result

    async def remove_collections(self, collect, for_delete=None):
        collection = self.db[collect]
        n = await collection.count_documents({})
        #print('*****************%s documents before calling delete_many()' % n)
        result = await collection.delete_many(for_delete)
        #print('*****************%s documents after' % (await collection.count_documents({})))

    async def insert_data(self, collect, data):
        collection = self.db[collect]
        result = await collection.insert_many(data)
        #print('*****************inserted %d docs' % (len(result.inserted_ids),))

    async def update_all(self, collect, data):
        """
          data = {filter: {}, data: {}}
        """
        collection = self.db[collect]
        result = await collection.update_many(
            data.get('filter', {}),
            {'$set': data.get('data', {})},
            upsert = True
        )
        """
        if result.modified_count:
            print(f'modified {data}')
        """
        #print('updated %s document' % result.modified_count)

    async def update_all_by_list(collect, data):
        """
          обновление по списку data = [{filter: {}, data: {}}...{}]
        """
        collection = self.db[collect]
        async for item in data:
            self.update_all(collect, item)


class PatientsListDB(CheredaDB):
    CLIENT_NAME_FOR_LOG = 'collection "patientsList"'
    db_log = logging.getLogger("scheduler.db.patientsList")

    def __init__(self):
        super(PatientsListDB, self).__init__()
        self.COL_NAME = self.COLLECTIONS['records']

    async def get_doubles(self, records):
        filt ={'$or': records}
        result = await self.by_filtering(self.COL_NAME, filt=filt)
        return result

    async def remove_doubles(self, params):
        for_delete = {'$or': params}
        await self.remove_collections(self.COL_NAME, for_delete)

    async def save_scheduler(self, data):
        await self.insert_data(self.COL_NAME, data)


class ReceptionRestDB(CheredaDB):
    """
      надстройка для работы сколлекцией receptionRest - там хранится 
      информация о перерывах
      запускать через менеджер контекста, потому что просто через __init__
      не инициализируется connection
    """
    CLIENT_NAME_FOR_LOG = 'collection "organizations"'
    db_log = logging.getLogger("scheduler.db.organizations")

    def __init__(self):
        super(ReceptionRestDB, self).__init__()
        self.COL_NAME = self.COLLECTIONS['rest']

    async def get_rest_list(self):
        """
          получает все активные перерывы
        """
        result = await self.by_filtering(self.COL_NAME, 
            filt={'deleted': {'$ne': True}})
        return result


class DoctorsDB(CheredaDB):
    """
      надстройка для работы сколлекцией doctors
      запускать через менеджер контекста, потому что просто через __init__
      не инициализируется connection
    """
    CLIENT_NAME_FOR_LOG = 'collection "doctors"'
    db_log = logging.getLogger("scheduler.db.doctors")

    def __init__(self):
        super(DoctorsDB, self).__init__()
        self.COL_NAME = self.COLLECTIONS['doctors']

    async def get_doctors(self, docname_list):
        filt ={'$or': [{'name': 
            {
                'first': "Галина",
                'middle': "Николаевна",
                'last': "Козина",
            }
        }]}
        filt ={'$or': docname_list}
        result = await self.by_filtering(self.COL_NAME, filt=filt)
        return result
        

class OrganizationsDB(CheredaDB):
    """
      надстройка для работы сколлекцией Организации
      запускать через менеджер контекста, потому что просто через __init__
      не инициализируется connection
    """
    CLIENT_NAME_FOR_LOG = 'collection "organizations"'
    db_log = logging.getLogger("scheduler.db.organizations")

    def __init__(self):
        super(OrganizationsDB, self).__init__()
        self.COL_NAME = self.COLLECTIONS['org']

    async def get_qms_id_list(self):
        KEY = 'qms_id'
        result = await self.by_filtering(self.COL_NAME, out = KEY)
        self.db_log.info(f'получено {len(result)} записей')
        return result

    async def qms_org_list(self, ids_list):
        """
          формируем список всех документов, у которых qms_id not None,
          тоесть всех, кто работает в QMS
        """
        result = await self.by_filtering(self.COL_NAME, 
            filt={'qms_id': {'$in': ids_list}})
        return result

#-------------Для timetable_online.py

class CheredaDbTt(CheredaDB):
    DB = conf.DATABASE_TT['default']
    CLIENT_NAME_FOR_LOG = 'client'
    COLLECTIONS = {
        'state': 'State', #аггрегированные данные
        'org': 'Organizations',
        'schedule': 'Schedule' #индивидуальные данные
        }
    db_log = logging.getLogger("timetable.db")


class OrganizationsDbTt(CheredaDbTt, OrganizationsDB):
    pass

class TimeTableDB(CheredaDbTt):

    async def save_scheduler_state(self, data):
        self.COL_NAME = self.COLLECTIONS['state']
        await self.insert_data(self.COL_NAME, data)

    async def save_scheduler_total(self, data):
        self.COL_NAME = self.COLLECTIONS['schedule']
        await self.insert_data(self.COL_NAME, data)

    async def update_records_state(self, data):
        """
          обновление записи в БД state, creiterion - условие , 
          по которому искать записи data - данные для обновления
        """
        self.COL_NAME = self.COLLECTIONS['state']
        await self.update_all(self.COL_NAME, data)

    async def update_records_schedule(self, data):
        """
          обновление записи в БД schedule, creiterion - условие , 
          по которому искать записи data - данные для обновления
        """
        self.COL_NAME = self.COLLECTIONS['schedule']
        await self.update_all(self.COL_NAME, data)

    async def records_list_by_dates(self, dates):
        """
          получаем выборку из бд, все записидля которых дата записи 
          принадлежит dates
        """
        self.COL_NAME = self.COLLECTIONS['schedule']
        result = await self.by_filtering(self.COL_NAME, 
            filt={'date_qms': {'$in': dates}})
        return result

    async def remove_doubles_state(self, params):
        self.COL_NAME = self.COLLECTIONS['state']
        for_delete = {'$or': params}
        await self.remove_collections(self.COL_NAME, for_delete)

    async def remove_doubles_schedule(self, params):
        self.COL_NAME = self.COLLECTIONS['schedule']
        for_delete = {'$or': params}
        await self.remove_collections(self.COL_NAME, for_delete)



