"""
import asyncio


async def print_every_second():
    "Print seconds"
    while True:
        for i in range(10):
            print(i, 's')
            await asyncio.sleep(1)


async def print_every_minute():
    for i in range(1, 2):
        await asyncio.sleep(10)
        print(i, 'minute')


loop = asyncio.get_event_loop()
loop.run_until_complete(
    asyncio.gather(print_every_second(),
                   print_every_minute())
)
loop.close()
"""

import asyncio
import time

async def firstWorker():
    while True:
        await asyncio.sleep(2)
        print("First Worker Executed")
        await test()

async def test():
    await asyncio.sleep(2)
    print('test')
        
async def secondWorker():
    while True:
        await asyncio.sleep(1)
        print("Second Worker Executed")


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(firstWorker())
    asyncio.ensure_future(secondWorker())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()