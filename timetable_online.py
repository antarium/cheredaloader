"""
  модуль отвечает за актуализацию расписания всех учреждений, которые 
  работают с qms и для которых через сервис череды работает онлайн расписание
"""
import os
import asyncio
import aiohttp
import datetime
import settings as conf
import warnings
import logging
import json
from scheduler import Mailer
import pandas as pd 
import random
import requests
import tempfile
import settings as conf
from prepare import get_dates, read_datafile, converted_dataframe, \
                    data_for_record, records_to_dataframe,\
                    compare_cache_with_df, get_dataframes_org, data_state_for_fccs
from cheredadb import OrganizationsDbTt, TimeTableDB


class Scheduler:
    root_log = logging.getLogger("timetable.root")
    timetable_log = logging.getLogger("timetable.log")
    error_upload_qms = [] #список организаций, для которых загрузка не удалась
    depth = 58 # глубина расписания
    cache = pd.DataFrame()

    def __init__(self):
        self.root_log.info('Инициализация планировщика')
        self.loop = asyncio.get_event_loop()

    async def update_state(self, conn, rec_state):
        for item in rec_state:
            await conn.update_records_state(item)

    async def update_schedule(self, conn, rec_schedule):
        for item in rec_schedule:
            await conn.update_records_schedule(item)

    async def processing_uploaded_data(self, org, date):
        """
          метод, в котором аггрегируются все действия по обработке расписания
        """
        #---блок костыль для fccs
        if org.get('qms_id', None) == 'fccs':
            datafile = 'tempfiles/fc_02_12.json'
            rec_state = await data_state_for_fccs(datafile)
            if rec_state:
                async with TimeTableDB() as conn:
                    await self.update_state(conn, rec_state)
            datafile, error, df = None, None, pd.DataFrame()
        elif org.get('qms_id', None) == 'vAE':
            datafile = await self.get_scheduler_spec(org, date)
            #datafile = 'tempfiles/kkb12.json'
        #----
        else:
            datafile = await self.get_scheduler(org.get('qms_id', ""), date)
        if datafile:
            df, error = await read_datafile(datafile, org, date)
            os.remove(datafile)
            #os.unlink(datafile)
        if error:
            self.error_upload_qms.append(f"{error} за дату {date}")
        if not df.empty and org.get('types', None):
            #работаем дальше
            df, error = await converted_dataframe(df, org['types'])
            if error:
                self.error_upload_qms.append(f"{error} за дату {date}")
            else:
                #df = await compare_cache_with_df(df, self.cache)
                if org.get('qms_id', None) == 'vDк' and date in ['20190502', '20190503', '20190510']:
                    print("Исключение")
                    return None
                if not df.empty:
                    rec_state, rec_schedule = await data_for_record(df)
                    #await compare_cache_with_df(rec_state, rec_schedule, None, None)
                    if rec_state and rec_schedule:
                        async with TimeTableDB() as conn:
                            await self.update_state(conn, rec_state)
                            #await self.update_schedule(conn, rec_schedule)

    async def get_scheduler_spec(self, org, date):
        """
          получение сетки расписания для учреждения за дату ТОЛЬКО ДЛЯ КРАЕВОЙ!!
          результат запроса записывается в файла, на выход путь к файлу 
        """ 
        headers = {'Content-type': 'application/json',  # Определение типа данных
                    'Accept': 'text/plain',
                    'Content-Encoding': 'utf-8'}
        #url = conf.URL_QMS_REQUEST
        file_path = None
        #data = {'qqc235': qms_id, 'date': date}
        data = org['request']
        qms_id = org['qms_id']
        data = json.loads(data)
        data['date'] = int(date)
        url = org['source']
        #answer = requests.post(url, data=json.dumps(data), headers=headers)
        connector = aiohttp.TCPConnector(ssl=False)
        async with aiohttp.ClientSession(connector=connector) as session:
            async with session.post(url, data=json.dumps(data, ensure_ascii=False).encode('utf-8'), 
                headers=headers, ssl=False) as resp:
                with tempfile.NamedTemporaryFile(delete=False, 
                    dir=conf.TEMP_PATH, suffix='.json', prefix=f'{qms_id}_') as f:
                    file_path = f.name
                    while True:
                        chunk = await resp.content.read(conf.QMS_CHUNK_SIZE)
                        if not chunk:
                            break
                        f.write(chunk)

        return file_path

    async def get_scheduler(self, qms_id, date):
        """
          получение сетки расписания для учреждения за дату
          результат запроса записывается в файла, на выход путь к файлу 
        """ 
        headers = {'Content-type': 'application/json',  # Определение типа данных
                    'Accept': 'text/plain',
                    'Content-Encoding': 'utf-8'}
        url = conf.URL_QMS_REQUEST
        file_path = None
        data = {'qqc235': qms_id, 'date': date}
        async with aiohttp.ClientSession() as session:
            async with session.post(url, data=json.dumps(data, ensure_ascii=False).encode('utf-8'), 
                headers=headers) as resp:
                with tempfile.NamedTemporaryFile(delete=False, 
                    dir=conf.TEMP_PATH, suffix='.json', prefix=f'{qms_id}_') as f:
                    file_path = f.name
                    while True:
                        chunk = await resp.content.read(conf.QMS_CHUNK_SIZE)
                        if not chunk:
                            break
                        f.write(chunk)
        return file_path

    async def update_timetables(self):
        """
          метод, отвечающий за получение и обновление расписани]
        """
        while True:
            print(f'==============={datetime.datetime.now()}===============')
            org_list = []
            async with OrganizationsDbTt() as conn:
                qms_ids = await conn.get_qms_id_list()
                #qms_ids = ['vDк']
                #qms_ids = ['fccs']
                #qms_ids = ['vAE']
                try:
                    qms_ids.remove('fccs')
                except Exception:
                    qms_ids = ['vDd', 'vDп', 'vDБ', 'vEI', 'vDк', 'vAE']
                #qms_ids = ['vDd']
                #qms_ids = ['fccs']
                org_list = await conn.qms_org_list(qms_ids)
            print(qms_ids)
            if not org_list:
                self.error_upload_qms.append('empty list org_list')
            else:
                dates = await get_dates(self.depth)
                #dates = ['20190610']
                #dates = ['20190610', '20190611', '20190612']
                coroutines = [self.processing_uploaded_data(org, date) 
                                for org in org_list for date in dates]
                completed, pending = await asyncio.wait(coroutines)
                
                #import ipdb; ipdb.set_trace()
            print(f'все полученные ошибки: {self.error_upload_qms}')
            print(f'==============={datetime.datetime.now()}===============')
            if conf.DEBUG_TT:
                break
            else:
                self.error_upload_qms = []
                await asyncio.sleep(conf.TIME_UPDATE_TT)
        self.loop.stop()

    def start(self):
        self.root_log.info('Запуск корутин планировщика')
        asyncio.ensure_future(self.update_timetables())
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            self.root_log.info('Работа сервиса остановлена по прерыванию')
        self.root_log.info('Работа сервиса остановлена')
        self.loop.stop()
        pending = asyncio.Task.all_tasks()
        self.loop.run_until_complete(asyncio.gather(*pending))
        self.loop.close()


if __name__ == '__main__':
    warnings.filterwarnings("ignore", message="numpy.dtype size changed")
    scheduler = Scheduler()
    scheduler.start()

