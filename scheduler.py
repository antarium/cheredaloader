"""
  Модуль отвечает за загрузку ежесуточную расписания для организаций, 
  работающих с Чередой, поддерживает возможность внеочередного запуска
  по запросу
"""
import os
import sys
import asyncio
import aiohttp
import datetime
import settings as conf
import motor.motor_asyncio
import pandas as pd
from pandas import ExcelFile
import numpy as np
import warnings
import logging
import random
import tempfile
from cheredadb import OrganizationsDB, ReceptionRestDB, DoctorsDB, \
                        PatientsListDB
import json
import smtplib
from prepare import *


class Mailer:
    """
      отвечает за отправку сообщений о краше на электронную почту
    """
    SENDER = 'cheredaloader@gmail.com'
    PASSWORD = 'noqsu2-jihxit-giHfud'
    #RECIPIENT = 'albatros.tan@yandex.ru' 
    RECIPIENT = ['albatros.tan@yandex.ru', 'cerbers13@yandex.ru']

    @classmethod
    def send_error_scheduler(cls, text):
        """
        subject = 'Scheduler error'
        smtp_server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        smtp_server.login(cls.SENDER, cls.PASSWORD)
        message = "Subject: {}\n\n{}".format(subject, text)
        smtp_server.sendmail(cls.SENDER, cls.RECIPIENT, message)
        smtp_server.close()
        """
        pass

    @classmethod
    def download_report(cls, text):
        """
        subject = 'Download REPORT CHEREDA'
        smtp_server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        smtp_server.login(cls.SENDER, cls.PASSWORD)
        message = "Subject: {}\n\n{}".format(subject, text)
        smtp_server.sendmail(cls.SENDER, cls.RECIPIENT, message.encode('utf-8'))
        smtp_server.close()
        """
        pass


class Server:
    mngr_log = logging.getLogger("scheduler.manager")

    def __init__(self):
        self.loop = asyncio.get_event_loop()

    async def handler(self, reader, writer):
        """
          отвечает за обработку входящих сообщений
        """
        data = await reader.read(100)
        message = data.decode()
        addr = writer.get_extra_info('peername')
        print("Received %r from %r" % (message, addr))

        print("Send: %r" % message)
        writer.write(data)
        await writer.drain()

        print("Close the client socket")
        writer.close()
        self.mngr_log.info(f'от {addr[0]}:{addr[1]} получена команда: {message}')
        self.state = message

    async def runserver(self):
        """
          является корутиной для веб сервера, через который осуществляется у
          правление планировщиком
        """
        self.mngr_log.info('Запуск вебсервера')
        server = await asyncio.start_server(
            self.handler, conf.MNRG_HOST, 
            conf.MNGR_PORT, loop=self.loop)

        addr = server.sockets[0].getsockname()
        self.mngr_log.info(f'Прослушивается: {addr}')

        with server:
            await server.serve_forever()


class Scheduler(Server):
    root_log = logging.getLogger("scheduler.root")
    scheduler_log = logging.getLogger("scheduler.log")
    error_upload_qms = False
    state = 'RUN'
    df_org = None
    df_rest = None
    df_doctors = None

    def __init__(self):
        self.root_log.info('Инициализация планировщика')
        self.loop = asyncio.get_event_loop()

    async def test(self, code):
        t = random.choice(range(1,5))
        print(f'test с ожиданием {t} код: {code}')
        await asyncio.sleep(t)
        print(f'test с ожиданием {t} код: {code} выполнен')

    async def json_to_dataframe(self, tempfile):
        df = pd.DataFrame()
        try:
            data = json.load(open(tempfile))
            #import ipdb;ipdb.set_trace()
            if data.get('data', None):
                #здесь преобразуем данные в Dataframe 
                df = pd.DataFrame.from_records(data['data'])
                #import ipdb; ipdb.set_trace()
            else:
                self.scheduler_log.warning(f'Для {tempfile} не обнаружено поля "data"')
                self.error_upload_qms = True
        except Exception as e:
            self.scheduler_log.error(
                    f'Не удалось прочитать и обработать файл: {tempfile}, ошибка: {e}')
            self.error_upload_qms = True
        finally:
            os.unlink(tempfile) #- удаление файла обязательно!!!
            return df

    async def prepare_data(self, tempfile, qms_id):
        """
          метод для обработки данных, записанных в файле tempfile
          собираем данные в датафрейм, здесь же вызываем методы для записи
          данных
        """
        if tempfile is None:
            return None
        df = await self.json_to_dataframe(tempfile)
        if not df.empty:
            #import ipdb;ipdb.set_trace()
            df, error = await to_filter_input_data(self.df_org, df, qms_id)
            if error:
                await self.call_exception('Не удалось отфильтровать расписание', 
                    'schedule filtering error', error)
            #import ipdb;ipdb.set_trace()
            df, error = await pretreatment_scheduler(df, viewOnly=None)
            if error:
                await self.call_exception('Не удалось подготовить расписание', 
                    'schedule pretreatment error', error)
            if df.empty:
                await self.call_exception('В полученных данных пусто', 
                    'The received data is empty', 'not data in field slots')
            #import ipdb;ipdb.set_trace()
            if not df.empty:
                async with DoctorsDB() as conn:
                    doctors_from_db = await conn.get_doctors(df['filter_find'].tolist())
                #import ipdb;ipdb.set_trace()
                self.scheduler_log.info(f'Для {qms_id} поолучено {df.shape[0]} \
                    записей, найдено соответствий в БД: {len(doctors_from_db)}')
                #import ipdb;ipdb.set_trace()
                df, error = await posttreatment_scheduler(df, doctors_from_db, self.df_rest)
                if error:
                    await self.call_exception('Не удалось обработать расписание', 
                        'schedule posttreatment error', error)
                #import ipdb;ipdb.set_trace()
                records = await search_dubles_request(df)
                if records is None:
                    await self.call_exception("нет данных", "data empty", "data empty")
                    return None
                async with PatientsListDB() as conn:
                    doubles = await conn.get_doubles(records)
                    #await conn.remove_doubles(records)
                    self.scheduler_log.info(f'Для {qms_id} обнаружено {len(doubles)}\
                        дублей')
                    to_save, to_remove = await preparing_schedule_for_recording(df, doubles)
                    if to_remove:
                        await conn.remove_doubles(to_remove)
                    if to_save:
                        await conn.save_scheduler(to_save)
                    else:
                        print(f"to_remove={to_remove}, to_save={to_save}")
                    self.scheduler_log.info(f'Для {qms_id} готово к записи {len(to_save)}')
                    """
                      временный блок для отправки отчета, позже надо будет 
                      сделать общий отчет по всем загрузкам!
                    """
                text = f'Task: QMS\nTo organize {qms_id} to load {len(to_save)} entries\nduplicates detected: {len(doubles)}'
                if len(doubles) == 0:
                    Mailer.download_report(text)
                else:
                    print(text)

                    #await conn.save_scheduler(to_save)

    async def survey_qms(self, qms_id):
        """
          опрос учреждений, ведущих расписание в qMS, qms_ids - список id
          по каждому из которых нужно сделать запрос и обработать его, 
          результат запроса записывается в файла, на выход путь к файлу 
        """
        headers = {'Content-type': 'application/json',  # Определение типа данных
                    'Accept': 'text/plain',
                    'Content-Encoding': 'utf-8'}
        url = conf.URL_QMS_REQUEST
        file_path = None
        data = {'qqc235': qms_id}
        #data = {'qqc235': qms_id, 'date': '20190522'}
        if qms_id == "vAE":
            #url = url
            url = conf.URL_TEMP
        async with aiohttp.ClientSession() as session:
            async with session.post(url, data=json.dumps(data, ensure_ascii=False).encode('utf-8'), 
                headers=headers) as resp:
                with tempfile.NamedTemporaryFile(delete=False, 
                    dir=conf.TEMP_PATH, suffix='.json', prefix=f'{qms_id}_') as f:
                    file_path = f.name
                    while True:
                        chunk = await resp.content.read(conf.QMS_CHUNK_SIZE)
                        if not chunk:
                            break
                        f.write(chunk) 
        return file_path, qms_id

    async def load_data_scheduler(self, qms_ids):
        try:
            coroutines = [self.survey_qms(item) for item in qms_ids]
            completed, pending = await asyncio.wait(coroutines)
            data_list = [item.result() for item in completed]
        except Exception as e:
            data_list = []
            await self.call_exception('Не удалось загрузить расписание', 
                'Error loading data', e)
        finally:
            return data_list

    async def scheduler_qms(self, qms_ids):
        """
          получение расписания для учреждений, работающих в qms за текущее
          число
        """
        #асинхронно скачиваем данные
        if conf.DEBUG_LOCAL:
            #запускаем обработку файлов
            data_list = [('data/vEI_53bj4x65.json', 'vEI'),
                        ('data/vDя_z7nmz9dz.json', 'vDя'),
                        ('data/vEJ_2nyey_sh.json', 'vEJ')]
            data_list = [('tempfiles/vEI_k0s2gt5c.json', 'vEI')]
        else:
            data_list = await self.load_data_scheduler(qms_ids)
        try:
            print(data_list)
            coroutines = [self.prepare_data(item[0], item[1]) for item in data_list]
            completed, pending = await asyncio.wait(coroutines)
        except Exception as e:
            await self.call_exception('Не удалось обработать расписание', 
                'Error preparing data', e)

        if self.error_upload_qms:
            text = "unable to load schedule for qms"
            Mailer.send_error_scheduler(text)
            self.error_upload_qms = False

    async def call_exception(self, log_msg, mail_msg, exc):
        """
          общий шаблон отработки исключений - письмо на емайл, запись в лог
        """
        self.scheduler_log.exception(f'{log_msg}')
        text = f'Task: {self.state}\n{mail_msg}: {exc}\n'
        Mailer.send_error_scheduler(text)

    async def scheduler(self):
        """
          корутина самого шедулера, все, что делает планировщик стартует 
          отсюда
        """
        self.scheduler_log.info('Запуск планировщика')
        print(f'Запуск планировщика, состояние: {self.state}')
        while True:
            await asyncio.sleep(conf.SCHEDULER_CHECK_PAUSE)
            if self.state == 'STOP':
                break
            elif self.state == 'SURVEY_ALL':
                #запустить опрос всех
                codes = [1,2,3,4,5]
                print('собираем таски')
                tasks = [self.loop.create_task(self.test(code)) for code in codes]
                print('таски собраны')
                if self.state == 'SURVEY_ALL':
                    self.state = 'RUN'#возвращаем RUN
            elif self.state == 'QMS':
                print(f'==============={datetime.datetime.now()}===============')
                self.scheduler_log.info('Выполняется команда "QMS"!')
                async with OrganizationsDB() as conn:
                    qms_ids = await conn.get_qms_id_list()
                    org_list = await conn.qms_org_list(qms_ids)
                errors_prepare = [] #ошибки во время подготовки данных
                self.df_org = await get_dataframes_org(org_list)
                if self.df_org.empty:
                    errors_prepare.append('Error read data from organizations')
                async with ReceptionRestDB() as conn:
                    rest_list = await conn.get_rest_list()
                self.df_rest, error = await rest_to_dataframe(rest_list)
                if error:
                    errors_prepare.append(error)
                #qms_ids = ['vDБ']
               	#qms_ids = ['vAE']
                #qms_ids = ["vEP"]
                #qms_ids = ["vEK"]
                try:
                    i = qms_ids.index("KKMIAC")
                    qms_ids.pop(i)
                except Exception:
                    pass
                print(qms_ids)
                print(errors_prepare)
                if not errors_prepare:
                    await self.scheduler_qms(qms_ids)

                else:
                    await self.call_exception(
                        'Ошибка при работе с БД, работа остановлена', 
                        'Error when working with the database, work stopped', 
                        ','.join(errors_prepare))
                #так как self.state мог изменить пока выполнялась задача проверяем
                if self.state == 'QMS':
                    self.state = 'RUN'#возвращаем RUN
                self.scheduler_log.info('Команда "QMS" выполнена!')
                print("========="*8)
                
        self.loop.stop()

    def start(self):
        self.root_log.info('Запуск корутин планировщика')
        asyncio.ensure_future(self.scheduler())
        asyncio.ensure_future(self.runserver())
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            self.root_log.info('Работа сервиса остановлена по прерыванию')
        self.root_log.info('Работа сервиса остановлена')
        self.loop.close()


if __name__ == '__main__':
    warnings.filterwarnings("ignore", message="numpy.dtype size changed")
    scheduler = Scheduler()
    scheduler.start()

