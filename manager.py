"""
  модуль для управления поведением планировщика путем отправления ему 
  управляющих команд
"""

import asyncio
import settings as conf
import argparse


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--send', 
        choices=['stop', 'log', 'all', 'qms', 'hospital']
    )
    #для вывода логов
    parser.add_argument('-l', '--lines', default=20, type=int)
    #тип лога для детализации какой именно лог показать - опциональный
    parser.add_argument('-d', '--detail', 
        choices=['manager', 'root', 'log', 'qms', 'hospital'], nargs='?'
    )
    return parser


class ManageScheduler:

    COMMANDS = {
        'stop': 'STOP',
        'log': 'VIEW_LOG',
        'all': 'SURVEY_ALL',
        'qms': 'QMS',
        'hospital': 'HOSP',
    }

    def __init__(self):
        self.loop = asyncio.get_event_loop()

    async def tcp_echo_client(self, message, loop):
        reader, writer = await asyncio.open_connection(
            conf.MNRG_HOST, 
            conf.MNGR_PORT,
            loop=loop
        )

        print('Send: %r' % message)
        writer.write(message.encode())

        data = await reader.read(100)

        writer.close()

    def send_message(self, message):
        """
          отправляет любое текстовое сообщение(для тестирования)
        """
        self.loop.run_until_complete(self.tcp_echo_client(message, self.loop))

    def __del__(self):
        print('buy')
        self.loop.close()


if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args()
    manager = ManageScheduler()
    if namespace.send == 'log':
        print('log')
    else:
        message = manager.COMMANDS[namespace.send]
        manager.send_message(message)



